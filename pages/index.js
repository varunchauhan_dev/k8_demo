import Head from "next/head";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Welcome to K8 deployment Demo</h1>

        <div className={styles.grid}>
          <a
            href="https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands"
            className={styles.card}
          >
            <h3>Documentation &rarr;</h3>
            <p>Find in-depth information about kubernetes commands</p>
          </a>

          <a
            href="https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/"
            className={styles.card}
          >
            <h3>Learn &rarr;</h3>
            <p>How to setup a bitbucket pipeline and deploy code?</p>
          </a>

          <a
            href="https://github.com/vercel/next.js/tree/master/examples"
            className={styles.card}
          >
            <h3>Examples &rarr;</h3>
            <p>Discover and deploy boilerplate example Next.js projects.</p>
          </a>

          <a
            href="https://cloud.google.com/kubernetes-engine/docs/how-to"
            className={styles.card}
          >
            <h3>Google Cloud &rarr;</h3>
            <p>
              How to setup k8 cluster, image registry and service account to
              deploy.
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  );
}
